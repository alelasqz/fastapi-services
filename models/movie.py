from config.database import Base
from sqlalchemy import Column, Integer, String, Float

class MovieModel(Base):
    __tablename__ = "movies"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    rate = Column(Float)
    year = Column(Integer)
    category = Column(String)