from config.database import Base
from sqlalchemy import Column, Integer, String, Float, ForeignKey, Text
from sqlalchemy.sql.sqltypes import Numeric

class AgencyModel(Base):
    __tablename__ = "agencies"

    idagencia = Column(Integer, primary_key=True, index=True)
    nombreagencia = Column(String(100))
    idstatus = Column(Integer)
    idnivel = Column(Integer)
    telefono1 = Column(String(20))
    telefono2 = Column(String(20), nullable=True)
    direccion = Column(String(200))
    ciudad = Column(String(100))
    estado = Column(String(100))
    logoagencia = Column(String(200), nullable=True)
    creditobase = Column(Float)
    creditoactual = Column(Float)
    banco = Column(String(100), nullable=True)
    cuenta = Column(String(50), nullable=True)
    beneficiario = Column(String(100), nullable=True)
    razonsocial = Column(String(100), nullable=True)
    identificadortributario = Column(String(50), nullable=True)
    idagenciapadre = Column(Integer, ForeignKey('agencies.idagencia'), nullable=True)
    comentario = Column(Text, nullable=True)
    acronimovoucher = Column(String(50), nullable=True)