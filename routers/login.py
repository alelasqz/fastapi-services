from fastapi import APIRouter
from schemas.user import User
from utils.jwt_manager import create_token
from fastapi.responses import JSONResponse

login_router = APIRouter()

@login_router.post('/login', tags=['auth'])
def login(user: User) -> str:
    if user.email == "string" and user.password == "string":
        token: str = create_token(user.model_dump())
        return JSONResponse(status_code=200, content=token)
    return JSONResponse(status_code=404, content={"message": "Usuario no válido"})
