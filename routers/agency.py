from fastapi import APIRouter, Path, Query, Depends
from schemas.agency import AgencySchema, AgencyCreate  # Deberás definir estos esquemas
from models.agency import AgencyModel  # Deberás definir este modelo
from config.database import Session
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder
from typing import List
from services.agency import AgencyService  # Deberás implementar este servicio
from utils.responses import error_response, confirm_response, data_response

agency_router = APIRouter()

@agency_router.get('/agencies', tags=['agencies'], response_model=List[AgencySchema], status_code=200)
def get_agencies() -> List[AgencySchema]:
    db = Session()
    result = AgencyService(db).get_agencies()
    if not result:
        return error_response(404, "No se encontraron registros")
    return data_response(200, jsonable_encoder(result))

@agency_router.get('/agency/{id}', tags=['agencies'], response_model=AgencySchema, status_code=200)
def get_agency(id: int = Path(ge=1, le=2000)) -> AgencySchema:
    db = Session()
    result = AgencyService(db).get_agency(id)
    if not result:
        return error_response(404, "No se encontró")
    return data_response(200, jsonable_encoder(result))

@agency_router.post('/agency', tags=['agencies'], response_model=dict, status_code=200)
def create_agency(agency: AgencyCreate) -> dict:
    db = Session()
    AgencyService(db).create_agency(agency)
    return confirm_response(200, "Agencia registrada satisfactoriamente")

@agency_router.put('/agency/{id}', tags=['agencies'], response_model=dict, status_code=200)
def update_agency(id: int, new_agency: AgencyCreate) -> dict:
    db = Session()
    old_agency = AgencyService(db).get_agency(id)
    if not old_agency:
        return error_response(404, "No se encontró")
    AgencyService(db).update_agency(old_agency, new_agency)
    return confirm_response(200, "Agencia editada satisfactoriamente")

@agency_router.delete('/agency/{id}', tags=['agencies'], response_model=dict, status_code=200)
def delete_agency(id: int) -> dict:
    db = Session()
    result = db.query(AgencyModel).filter(AgencyModel.id == id).first()
    if not result:
        return error_response(404, "No se encontró")
    AgencyService(db).delete_agency(id)
    return confirm_response(200, "Agencia eliminada satisfactoriamente")
