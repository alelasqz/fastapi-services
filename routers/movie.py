from fastapi import APIRouter, Path, Query, Depends
from schemas.movie import MovieSchema, MovieCreate
from models.movie import MovieModel
from config.database import Session
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder
from middlewares.jwt_bearer import JWTBearer
from typing import List
from services.movie import MovieService
from utils.responses import error_response, confirm_response, data_response

movie_router = APIRouter()

@movie_router.get('/movies', tags=['movies'], response_model=List[MovieSchema], status_code=200)
def get_movies() -> List[MovieSchema]:
    db = Session()
    result = MovieService(db).get_movies()
    print(result)
    if not result:
        return error_response(404,"No se encontraron registros")
    return data_response(200, jsonable_encoder(result))

# @movie_router.get('/movies/{id}', tags=['movies'], response_model=MovieSchema, status_code=200, dependencies=[Depends(JWTBearer())])
@movie_router.get('/movie/{id}', tags=['movies'], response_model=MovieSchema, status_code=200)
def get_movie(id: int = Path(ge=1, le=2000)) -> MovieSchema:
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return error_response(404,"No se encontró")
    return data_response(200, jsonable_encoder(result))

@movie_router.get('/movies/', tags=['movies'], response_model=List[MovieSchema], status_code=200)
def get_movies_by_category(category: str = Query(min_length=1, max_length=10)) -> List[MovieSchema]:
    db = Session()
    result = MovieService(db).get_movies_by_category(category)
    if not result:
        return error_response(404,"No se encontró")
    return data_response(200, jsonable_encoder(result))

@movie_router.post('/movie', tags=['movies'], response_model=dict, status_code=200)
def create_movie(movie: MovieCreate) -> dict:
    db = Session()
    MovieService(db).create_movie(movie)
    return confirm_response(200, "Película registrada satisfactoriamente")

@movie_router.put('/movie/{id}', tags=['movies'], response_model=dict, status_code=200)
def update_movie(id: int, new_movie: MovieCreate) -> dict:
    db = Session()
    old_movie = MovieService(db).get_movie(id)
    if not old_movie:
        return error_response(404,"No se encontró")
    MovieService(db).update_movie(old_movie, new_movie)
    return confirm_response(200, "Película editada satisfactoriamente")
    
@movie_router.delete('/movie/{id}', tags=['movies'], response_model=dict, status_code=200)
def delete_movie(id: int) -> dict:
    db = Session()
    result = db.query(MovieModel).filter(MovieModel.id == id).first()
    if not result:
        return error_response(404,"No se encontró")
    MovieService(db).delete_movie(id)
    return confirm_response(200, "Película eliminada satisfactoriamente")