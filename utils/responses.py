from fastapi.responses import JSONResponse

def error_response(status_code: int, message: str):
    return JSONResponse(status_code=status_code, content={"result": [], "error": True, "message": message})

def confirm_response(status_code: int, message: str):
    return JSONResponse(status_code=status_code, content={"result": [], "error": False, "message": message})

def data_response(status_code: int, data: str):
    return JSONResponse(status_code=status_code, content={"result": data, "error": False, "message": ""})