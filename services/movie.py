from models.movie import MovieModel
from schemas.movie import MovieSchema, MovieCreate
from fastapi.responses import JSONResponse

class MovieService():
    def __init__(self, db) -> None:
        self.db = db

    def get_movies(self):
        result = self.db.query(MovieModel).all()
        return result
    
    def get_movie(self, id: int):
        result = self.db.query(MovieModel).filter(MovieModel.id == id).first()
        return result
    
    def get_movies_by_category(self, category: str):
        result = self.db.query(MovieModel).filter(MovieModel.category == category).all()
        return result
    
    def create_movie(self, movie: MovieCreate):
        new_movie = MovieModel(**movie.dict())
        self.db.add(new_movie)
        self.db.commit()
        return
    
    def update_movie(self, old_movie: MovieCreate, new_movie: MovieCreate):
        old_movie.name = new_movie.name
        old_movie.rate = new_movie.rate
        old_movie.year = new_movie.year
        old_movie.category = new_movie.category
        self.db.commit()
        return
    
    def delete_movie(self, id: int):
        self.db.query(MovieModel).filter(MovieModel.id == id).delete()
        self.db.commit()
        return