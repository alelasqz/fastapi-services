from models.agency import AgencyModel
from schemas.agency import AgencySchema, AgencyCreate
from sqlalchemy.orm import Session

class AgencyService():
    def __init__(self, db: Session) -> None:
        self.db = db

    def get_agencies(self):
        result = self.db.query(AgencyModel).all()
        return result
    
    def get_agency(self, idagencia: int):
        result = self.db.query(AgencyModel).filter(AgencyModel.idagencia == idagencia).first()
        return result
    
    def create_agency(self, agency: AgencyCreate):
        new_agency = AgencyModel(**agency.dict())
        self.db.add(new_agency)
        self.db.commit()
        return
    
    def update_agency(self, idagencia: int, new_data: AgencyCreate):
        old_agency = self.db.query(AgencyModel).filter(AgencyModel.idagencia == idagencia).first()
        if old_agency:
            update_data = new_data.dict(exclude_unset=True)
            for key, value in update_data.items():
                setattr(old_agency, key, value)
            self.db.commit()
            return True
        return False
    
    def delete_agency(self, idagencia: int):
        result = self.db.query(AgencyModel).filter(AgencyModel.idagencia == idagencia).delete()
        self.db.commit()
        return result
