from pydantic import BaseModel, Field
from typing import Optional

class AgencyBase(BaseModel):
    nombreagencia: str = Field(..., min_length=1, max_length=100)
    idstatus: int = Field(default=1)
    idnivel: int = Field(...)
    telefono1: str = Field(..., min_length=7, max_length=20)
    telefono2: Optional[str] = Field(None, min_length=7, max_length=20)
    direccion: str = Field(..., min_length=1, max_length=200)
    ciudad: str = Field(..., min_length=1, max_length=100)
    estado: str = Field(..., min_length=1, max_length=100)
    logoagencia: Optional[str] = Field(None, min_length=1, max_length=200)
    creditobase: float = Field(...)
    creditoactual: float = Field(...)
    banco: Optional[str] = Field(None, min_length=1, max_length=100)
    cuenta: Optional[str] = Field(None, min_length=1, max_length=50)
    beneficiario: Optional[str] = Field(None, min_length=1, max_length=100)
    razonsocial: Optional[str] = Field(None, min_length=1, max_length=100)
    identificadortributario: Optional[str] = Field(None, min_length=1, max_length=50)
    idagenciapadre: Optional[int] = Field(None)
    comentario: Optional[str] = Field(None, min_length=1, max_length=500)
    acronimovoucher: Optional[str] = Field(None, min_length=1, max_length=50)

class AgencyCreate(AgencyBase):
    pass

class AgencySchema(AgencyBase):
    idagencia: Optional[int] = Field(None)

    class Config:
        orm_mode = True
