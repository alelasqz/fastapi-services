from pydantic import BaseModel, Field
from typing import Optional

class MovieBase(BaseModel):
    name: str = Field(default="Película ", min_length=5, max_length=50)
    rate: float = Field(default= 10, ge=1, le=10)
    year: int = Field(default=2024, le=2024)
    category: str = Field(default="Romance",min_length=5, max_length=50)

class MovieCreate(MovieBase):
    pass

class MovieSchema(MovieBase):
    id: Optional[int] = None

    class Config:
        orm_mode = True

    
