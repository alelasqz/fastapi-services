from fastapi import FastAPI
from config.database import engine, Base
from middlewares.error_handler import ErrorHandler
from routers.movie import movie_router
from routers.login import login_router
from routers.agency import agency_router

app = FastAPI()
app.title = 'fastapi-services' 
app.version = '0.0.1'

Base.metadata.create_all(bind=engine)

app.add_middleware(ErrorHandler)
app.include_router(movie_router)
app.include_router(login_router)
app.include_router(agency_router)

